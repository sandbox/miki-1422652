<?php
/**
 * @file
 * Freightquote administration menu items.
 */

/**
 * Freightquote.com settings.
 *
 * Records Freightquote account information neccessary to use the service. Allows testing
 * or production mode.
 *
 * @see uc_admin_settings_validate()
 * @see uc_admin_settings_submit()
 * @ingroup forms
 */
function uc_freightquote_admin_settings() {
  // Server url 
  $form['uc_freightquote_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#default_value' => variable_get('uc_freightquote_server', 'https://b2b.freightquote.com/WebService/QuoteService.asmx'),
  );
  
  // Container for credential forms
  $form['freightquote_credentials'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Credentials'),
    '#description' => t('Account number and authorization information.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['freightquote_credentials']['uc_freightquote_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter your Freightquote.com Username'),
    '#default_value' => variable_get('uc_freightquote_username', 'xmltest@freightquote.com'),
    '#required' => TRUE,
  );
  
  $form['freightquote_credentials']['uc_freightquote_password'] = array(
    '#type' => 'password',
    '#title' => 'FreightQuote password',
    '#description' => t('Leave blank to keep the old value.'),
    '#default_value' => '',
  );
  
  // Service options
  $form['freightquote_options'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Options'),
    '#description'   => t('Service options.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  
  $service_types = array('LTL', 'Truckload', 'Groupage', 'Haulage', 'All');
  $service_types = drupal_map_assoc($service_types);
  
  $form['freightquote_options']['uc_freightquote_service_type'] = array(
    '#type' => 'select',
    '#title' => t('Service Type'),
    '#description' => t('What service type do you want to use?'),
    '#options' => $service_types,
    '#default_value' => variable_get('uc_freightquote_service_type', 'LTL'),
  );
  
  $form['freightquote_options']['uc_freightquote_origin_loading_dock'] = array(
    '#type' => 'radios',
    '#title' => t('Shipping Origin: Loading Dock'),
    '#description' => t('Does the shipping location have a loading dock?'),
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => variable_get('uc_freightquote_origin_loading_dock', 'true'),
  );
  
  $form['freightquote_options']['uc_freightquote_origin_residence'] = array(
    '#type' => 'radios',
    '#title' => t('Shipping Origin: Residence'),
    '#description' => t('Is the shipping location a residence?'),
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => variable_get('uc_freightquote_origin_residence', 'false'),
  );
  
  $form['freightquote_options']['uc_freightquote_origin_construction_site'] = array(
    '#type' => 'radios',
    '#title' => t('Shipping Origin: Construction Site'),
    '#description' => t('Is the shipping location a construction site?'),
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => variable_get('uc_freightquote_origin_construction_site', 'false'),
  );
  
  $form['freightquote_options']['uc_freightquote_blind_ship'] = array(
    '#type' => 'radios',
    '#title' => t('Blind Ship'),
    '#description' => t('Blind shipments are used to keep the originating location and receiving destination unaware of each other.'),
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => variable_get('uc_freightquote_blind_ship', 'false'),
  );
  
  /*
   RESEARCH THIS
  
  $form['services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Service options'),
    '#description' => t('Set the conditions that will return a Freightquote quote.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  if (module_exists('rules')) {
  $form['services']['service_conditions'] = array(
  '#type' => 'fieldset',
  '#title' => t('Freightquote service conditions'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
  );
  $conditions = rules_config_load('get_quote_from_ups');
  if ($conditions) {
  RulesPluginUI::$basePath = 'admin/config/workflow/rules/components';
  $conditions->form($form['services']['service_conditions'], $form_state);
  }
  }*/
  
  // Container for quote options
  $form['freightquote_quote_options'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Quote options'),
    '#description' => t('Preferences that affect computation of quote.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  // Form to select package types
  /**
   * TODO: Remove, not needed since this is set per product.
   */
  /*
  $form['freightquote_quote_options']['freightquote_pkg_type'] = array(
    '#type' => 'select',
    '#title' => t('Default Package Type'),
    '#default_value' => variable_get('freightquote_pkg_type', _freightquote_pkg_types()),
    '#options' => _freightquote_pkg_types(),
    '#description' => t('Type of packaging to be used.  May be overridden on a per-product basis via the product node edit form.'),
  );*/
  
  $form['freightquote_quote_options']['uc_freightquote_HazardousMaterialContactName'] = array(
  	'#type' => 'textfield',
    '#required' => TRUE,
  	'#title' => t('The name of the hazardous materials contact'),
    '#description' => t('This parameter is required by freightquote service.'),
    '#default_value' => variable_get('uc_freightquote_HazardousMaterialContactName', ''),
  );
    
  $form['freightquote_quote_options']['uc_freightquote_HazardousMaterialContactPhone'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('The phone number for the hazardous materials contact'),
    '#description' => t('This parameter is required by freightquote service.'),
    '#default_value' => variable_get('uc_freightquote_HazardousMaterialContactPhone', ''),
  );
  
  // Container for markup forms
  $form['freightquote_markups'] = array(
    '#type' => 'fieldset',
    '#title' => t('Markups'),
    '#description' => t('Modifiers to the shipping weight and quoted rate.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  // Form to select type of rate markup
  $form['freightquote_markups']['uc_freightquote_markup_type'] = array(
    '#type' => 'select',
    '#title' => t('Rate markup type'),
    '#default_value' => variable_get('uc_freightquote_markup_type', '%'),
    '#options' => array(
      '%' => t('Percentage (%)'),
      'x' => t('Multiplier (×)'),
      '$' => t('Addition (!currency)', array('!currency' => variable_get('uc_currency_sign', '$'))),
    ),
  );
  
  // Form to select rate markup amount
  $form['freightquote_markups']['uc_freightquote_markup'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping rate markup'),
    '#default_value' => variable_get('uc_freightquote_markup', '0'),
    '#description' => t('Markup shipping rate quote by currency amount, percentage, or multiplier.'),
  );
  
  // Taken from system_settings_form(). Only, don't use its submit handler.
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  
  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }
  
  return $form;
}

/**
* Validation handler for freightquote_admin_settings.
*
* Requires password only if it hasn't been set.
*
* @see freightquote_admin_settings()
* @see freightquote_admin_settings_submit()
*/
function uc_freightquote_admin_settings_validate($form, &$form_state) {
  // Check if password is set or we have empty pass.
  $old_password = variable_get('uc_freightquote_password', 'XML');
  if (!$form_state['values']['uc_freightquote_password']) {
    if ($old_password) {
      form_set_value($form['freightquote_credentials']['uc_freightquote_password'], $old_password, $form_state);
    }
    else {
      form_set_error('uc_freightquote_password', t('Password field is required.'));
    }
  }

  // Check rate
  if (!is_numeric($form_state['values']['uc_freightquote_markup'])) {
    form_set_error('freightquote_rate_markup', t('Rate markup must be a numeric value.'));
  }
}

/**
 * Submit handler for settings form.
 */
function uc_freightquote_settings_form_submit($form, &$form_state) {
  $fields = array(
    'uc_freightquote_username',
    'uc_freightquote_password',
    'uc_freightquote_service_type',
    'uc_freightquote_origin_loading_dock',
    'uc_freightquote_origin_residence',
    'uc_freightquote_origin_construction_site',
    'uc_freightquote_blind_ship',
    'uc_freightquote_server',
    'uc_freightquote_HazardousMaterialContactName',
    'uc_freightquote_HazardousMaterialContactPhone',
    'uc_freightquote_markup_type',
    'uc_freightquote_markup',
  );
  
  foreach ($fields as $key) {
    $value = $form_state['values'][$key];
  
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }
  
  drupal_set_message(t('The configuration options have been saved.'));
  
  cache_clear_all();
  drupal_theme_rebuild();
}